package com.noveogroup.training.data.data.network;


import com.noveogroup.trainig.fragments.R;
import com.noveogroup.training.data.data.model.News;
import com.noveogroup.training.data.data.model.NewsDetails;
import dagger.Provides;
import org.slf4j.LoggerFactory;
import retrofit2.HttpException;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

@Singleton
public class Api {

    private static final int ERROR500 = 500;
    private static final int ERROR404 = 404;
    private NewsService newsService;

    @Inject
    public Api(NewsService newsService) {
        this.newsService = newsService;
    }

    public static int getErrorText(Throwable error) {
        if (error instanceof HttpException) {
            switch (((HttpException) error).code()) {
                case ERROR500:
                    return R.string.error500;
                case ERROR404:
                    return R.string.error404;
                default:
                    return R.string.error_strange;
            }
        }
        if (error instanceof TimeoutException) {
            return R.string.error_time;
        }
        return R.string.error_strange;
    }

    public Observable<List<News>> getAll() {
        return newsService.getAll()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public Observable<NewsDetails> getNewsById(String newsId) {
        return newsService.getNewsById(newsId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
