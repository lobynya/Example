package com.noveogroup.training.data.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import com.noveogroup.training.data.ui.fragment.BaseFragment;
import com.noveogroup.training.data.util.FragmentUtils;

import rx.Subscriber;

@SuppressWarnings("ConstantConditions")
public abstract class BaseActivity extends AppCompatActivity {
    @LayoutRes
    protected abstract int getLayoutId();

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        final Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            onHandleExtras(intent.getExtras());
        }

        super.onCreate(savedInstanceState);
        themeInit();
        setContentView(getLayoutId());
        ButterKnife.bind(this);
    }

    public void themeInit() {

    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
            return;
        }

        finish();
    }

    public void subscribe(Subscriber subscriber) {

    }

    public void unSubscribe(Subscriber subscriber) {

    }

    public void refresh() {
    }

    /**
     * Called from #onCreate(Bundle) in case of not null #getIntent().getExtras().
     *
     * @param extras - result from #getIntent().
     */
    protected void onHandleExtras(@NonNull final Bundle extras) {
        //do nothing, override if you need handle arguments
    }

    public void replaceFragment(final BaseFragment nextFragment, final boolean replace,
                                final FragmentUtils.AnimationType animation) {
        throw new UnsupportedOperationException("stub!");
    }

}
