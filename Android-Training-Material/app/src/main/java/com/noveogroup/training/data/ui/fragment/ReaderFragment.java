package com.noveogroup.training.data.ui.fragment;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.*;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.support.v4.widget.SwipeRefreshLayout;
import android.transition.Transition;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.noveogroup.trainig.fragments.R;
import com.noveogroup.training.data.data.controller.DetailNewsController;
import com.noveogroup.training.data.data.model.NewsDetails;
import com.noveogroup.training.data.data.network.Api;
import com.noveogroup.training.data.data.preference.Favorite;
import com.noveogroup.training.data.di.Injector;
import com.noveogroup.training.data.ui.activity.GalleryActivity;
import com.noveogroup.training.data.util.FragmentUtils;
import com.noveogroup.training.data.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import javax.inject.Inject;
import java.util.PriorityQueue;
import java.util.Queue;

/*
 * Created by alobynya on 10-Apr-17.
 */
public class ReaderFragment extends BaseFragment {
    private static final String NEWS_KEY = "com.noveogroup.training.fragments.readerFragments.key";
    private static final String IMAGE_KEY = "com.noveogroup.training.fragments.readerFragments.image";
    @Inject
    DetailNewsController detailNewsController;
    @Inject
    Favorite favorite;
    @BindView(R.id.reader_text)
    TextView textView;
    @BindView(R.id.reader_date)
    TextView dateView;
    @BindView(R.id.image_view)
    ImageView imageView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.lock_layout)
    RelativeLayout lockLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.app_bar)
    AppBarLayout appBarLayout;
    @BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.floating_button)
    FloatingActionButton floatingActionButton;
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String newsId;
    private String image;
    private Subscription subscription;
    private ReaderFragmentController controller;

    public static ReaderFragment newInstance(@NonNull String newsId, @Nullable String image) {
        final Bundle arguments = new Bundle();
        arguments.putString(NEWS_KEY, newsId);
        arguments.putString(IMAGE_KEY, image);
        return FragmentUtils.withArgs(new ReaderFragment(), arguments);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Injector.getReaderComponent().inject(this);
        controller = new ReaderFragmentController(getBaseActivity().getWindow().getEnterTransition());
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        int color = ContextCompat.getColor(getContext(),
                Utils.getColor(getContext(), R.attr.colorPrimary));
        swipeRefresh.setColorSchemeColors(color);
        progressBar.getIndeterminateDrawable().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        swipeRefresh.setOnRefreshListener(() -> {
            getNewsDetails(true);
            changeState(FragmentState.LOAD);
        });
        getBaseActivity().setSupportActionBar(toolbar);
        getBaseActivity().getSupportActionBar().setHomeButtonEnabled(true);
        getBaseActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getBaseActivity().getSupportActionBar().setTitle("");
        floatingActionButton.setColorFilter(Color.WHITE);
        getBaseActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.transperent_light));
        if (image != null) {
            SimpleTarget<Bitmap> target = new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {
                    if (appBarLayout != null) {
                        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
                        Point size = new Point();
                        getBaseActivity().getWindowManager().getDefaultDisplay().getSize(size);
                        lp.height = size.x * bitmap.getHeight() / bitmap.getWidth();
                        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        imageView.setImageBitmap(bitmap);
                    }
                }
            };
            Glide.with(getContext().getApplicationContext())
                    .load(image)
                    .asBitmap()
                    .error(R.drawable.ic_stub_error)
                    .into(target);
        } else {
            appBarLayout.setFitsSystemWindows(false);
        }
        if (favorite.contains(newsId)) {
            floatingActionButton.setImageResource(R.drawable.ic_like_solid);
        }
        floatingActionButton.setOnClickListener(view1 -> {
            if (favorite.contains(newsId)) {
                favorite.deleteNews(newsId);
                floatingActionButton.setImageResource(R.drawable.ic_like_border);
                showSnack(R.string.favorite_del);
            } else {
                favorite.addNews(newsId);
                floatingActionButton.setImageResource(R.drawable.ic_like_solid);
                showSnack(R.string.favorite_add);
            }
        });
        collapsingToolbarLayout.setExpandedTitleColor(Color.TRANSPARENT);
        appBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {

            if (verticalOffset == 0) {
                swipeRefresh.setEnabled(true);
            } else {
                swipeRefresh.setEnabled(false);
            }
        });
        floatingActionButtonShow(floatingActionButton);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                controller.push(Observable.just(floatingActionButton)
                        .doOnNext(floatingActionButton1 -> floatingActionButtonHide()));
                new Handler().postDelayed(() -> controller.push(Observable.just(2)
                                .doOnNext(h -> getBaseActivity().supportFinishAfterTransition())),
                        getContext().getResources().getInteger(R.integer.animation_time));
                return true;
            }
            return false;
        }));
    }

    @Override
    public void onDestroyView() {
        if (subscription != null)
            subscription.unsubscribe();
        floatingActionButton.hide();
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        changeState(FragmentState.LOAD);
        getNewsDetails(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                controller.push(Observable.just(floatingActionButton)
                        .doOnNext(floatingActionButton1 -> floatingActionButtonHide()));
                new Handler().postDelayed(() -> controller.push(Observable.just(2)
                                .doOnNext(h -> getBaseActivity().supportFinishAfterTransition())),
                        getContext().getResources().getInteger(R.integer.animation_time));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void floatingActionButtonShow(FloatingActionButton floatingActionButton) {
        floatingActionButton.animate().cancel();//cancel all animations
        floatingActionButton.setScaleX(0f);
        floatingActionButton.setScaleY(0f);
        floatingActionButton.setAlpha(0f);
        floatingActionButton.setVisibility(View.VISIBLE);
        floatingActionButton.animate().setDuration(getContext().getResources().getInteger(R.integer.animation_time))
                .setStartDelay(getContext().getResources().getInteger(R.integer.animation_time) * 2)
                .scaleX(1)
                .scaleY(1)
                .alpha(1)
                .setInterpolator(new LinearOutSlowInInterpolator());
    }

    private void floatingActionButtonHide() {
        floatingActionButton.animate().cancel();//cancel all animations
        floatingActionButton.animate().setStartDelay(0);
        floatingActionButton.hide();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_reader;
    }

    @Override
    protected void onHandleArguments(@NonNull final Bundle arguments) {
        newsId = arguments.getString(NEWS_KEY);
        image = arguments.getString(IMAGE_KEY);
    }


    private void getNewsDetails(boolean refresh) {
        Observable<NewsDetails> observable = detailNewsController.getNewsDetails(newsId, refresh);
        subscription = observable
                .subscribe(newsDetails -> controller.push(observable.doOnNext(newsDetails1 -> {
                    changeState(FragmentState.READ);
                    bindNewsView(newsDetails);
                })), error -> {
                    changeState(FragmentState.ERROR);
                    textView.setText(error.getMessage());
                    showSnack(error);
                });
    }

    private void bindNewsView(NewsDetails newsDetails) {
        getBaseActivity().getSupportActionBar().setTitle(newsDetails.getTitle());
        textView.setText(Utils.fromHtml(newsDetails.getDescription()));
        dateView.setText(Utils.dateToString(newsDetails.getDate()));
        if (newsDetails.getImage() != null) {
            imageView.setOnClickListener(v -> startActivity(GalleryActivity.newInstance(getContext(),
                    newsDetails.getImage(),
                    newsDetails.getTitle())));
        } else {
            appBarLayout.setExpanded(false, false);
            imageView.setImageResource(0);
            CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
            lp.height = toolbar.getHeight();
            appBarLayout.setLayoutParams(lp);
            collapsingToolbarLayout.setTitleEnabled(false);
            toolbar.setTitle(newsDetails.getTitle());
        }
    }

    private void showSnack(Throwable error) {
        showSnack(Api.getErrorText(error),
                R.string.repeat,
                v -> {
                    getNewsDetails(true);
                    changeState(FragmentState.LOAD);
                });
    }

    private void showSnack(@StringRes int text) {
        Snackbar snackbar = Snackbar.make(getView(), text, Snackbar.LENGTH_LONG);
        ((TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text))
                .setTextColor(Color.WHITE);
        snackbar.show();
    }

    private void changeState(FragmentState newState) {
        if (newState != state) {
            switch (newState) {
                case LOAD:
                    lockLayout.setVisibility(View.VISIBLE);
                    break;
                case READ:
                    lockLayout.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                    swipeRefresh.setRefreshing(false);
                    break;
                case ERROR:
                    lockLayout.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                    swipeRefresh.setRefreshing(false);
                    break;
            }
        }
    }

    private class ReaderFragmentController {
        private volatile Queue<Observable> queue;
        volatile boolean ready = false;

        ReaderFragmentController(@Nullable Transition enterTransition) {
            logger.debug(enterTransition+"");
            if (!enterTransition.getTargets().isEmpty()){//fixME not working
                ready=true;
            } else {
                logger.debug("LISTEN");
                enterTransition.addListener(new Transition.TransitionListener() {
                    @Override
                    public void onTransitionStart(final Transition transition) {

                    }

                    @Override
                    public void onTransitionEnd(final Transition transition) {
                        logger.debug("READY");
                        ready = true;
                        if (!queue.isEmpty()) next();

                    }

                    @Override
                    public void onTransitionCancel(final Transition transition) {

                    }

                    @Override
                    public void onTransitionPause(final Transition transition) {

                    }

                    @Override
                    public void onTransitionResume(final Transition transition) {

                    }
                });
            }
            queue = new PriorityQueue<>();
        }

        synchronized
        void push(Observable newsDetails) {
            queue.add(newsDetails);
            if (ready) {
                ready = false;
                next();
            }
        }

        synchronized
        private void next() {
            queue.remove().subscribe(object -> {
                if (queue.isEmpty()) {
                    ready = true;
                } else {
                    next();
                }
            });
        }
    }
}
