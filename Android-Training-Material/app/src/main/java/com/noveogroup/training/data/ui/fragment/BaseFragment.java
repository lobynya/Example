package com.noveogroup.training.data.ui.fragment;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.FrameStats;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.noveogroup.trainig.fragments.R;
import com.noveogroup.training.data.data.network.Api;
import com.noveogroup.training.data.ui.activity.BaseActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.noveogroup.training.data.util.Utils;

public abstract class BaseFragment extends Fragment {
    private Unbinder unbinder;
    protected FragmentState state;
    @LayoutRes
    protected abstract int getLayoutId();

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        final Bundle arguments = getArguments();
        if (arguments != null) {
            onHandleArguments(arguments);
        }
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        final View root = inflater.inflate(getLayoutId(), container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    /**
     * Called from #onCreate(Bundle) in case of not null #getArguments().
     *
     * @param arguments - result from #getArguments().
     */
    protected void onHandleArguments(@NonNull final Bundle arguments) {
        //do nothing, override if you need handle arguments
    }

    protected BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    protected void showToast(final String text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
    }

    protected void showToast(@StringRes final int stringId) {
        Toast.makeText(getContext(), stringId, Toast.LENGTH_SHORT).show();
    }

    protected Snackbar showSnack(@StringRes int text, @StringRes int actionText, @Nullable View.OnClickListener listener) {
        Snackbar snackbar = Snackbar
                .make(getView(), text, Snackbar.LENGTH_LONG)
                .setAction(getContext().getString(actionText), listener)
                .setActionTextColor(ContextCompat.getColor(getContext(),
                        Utils.getColor(getContext(), R.attr.colorPrimary)));
        ((TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text))
                .setTextColor(Color.WHITE);
        snackbar.show();
        return snackbar;
    }

    protected enum FragmentState {
        READ, LOAD, START, ERROR
    }


}
