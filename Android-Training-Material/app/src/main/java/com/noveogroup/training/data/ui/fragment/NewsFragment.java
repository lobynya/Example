package com.noveogroup.training.data.ui.fragment;

import android.app.ActivityOptions;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.view.*;

import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.noveogroup.trainig.fragments.R;
import com.noveogroup.training.data.data.controller.NewsController;
import com.noveogroup.training.data.data.model.News;
import com.noveogroup.training.data.data.model.Topic;
import com.noveogroup.training.data.data.network.Api;
import com.noveogroup.training.data.data.preference.Favorite;
import com.noveogroup.training.data.ui.activity.ReaderActivity;
import com.noveogroup.training.data.ui.adapter.ItemClickSupport;
import com.noveogroup.training.data.ui.adapter.NewsAdapter;
import com.noveogroup.training.data.di.Injector;
import com.noveogroup.training.data.util.Utils;
import com.noveogroup.training.data.util.FragmentUtils;

import butterknife.BindView;
import org.slf4j.LoggerFactory;
import rx.Subscription;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


public class NewsFragment extends BaseFragment {

    private static final String ARGUMENT_KEY = "com.noveogroup.training.fragments.newsFragments.key";
    @Inject
    NewsController newsController;
    @Inject
    Favorite favorite;
    @BindView(R.id.news_list)
    RecyclerView newsListView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.lock_layout)
    RelativeLayout lockLayout;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.error_text)
    TextView errorText;
    private NewsAdapter adapter;
    private Topic topic;
    private Subscription subscription;
    private Snackbar snackbar;

    public static NewsFragment newInstance(final int topicId) {
        final Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_KEY, topicId);
        return FragmentUtils.withArgs(new NewsFragment(), arguments);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Injector.getAppComponent().inject(this);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getNews(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        newsListView.setLayoutManager(layoutManager);
        int color = ContextCompat.getColor(getContext(),
                Utils.getColor(getContext(), R.attr.colorPrimary));
        progressBar.getIndeterminateDrawable().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        getBaseActivity().getSupportActionBar().setTitle(topic.title);
        swipeRefreshLayout.setColorSchemeColors(color);
        if (adapter == null) {
            adapter = new NewsAdapter(getContext(), new ArrayList<>(), favorite);
        }
        newsListView.setAdapter(adapter);

        ItemClickSupport.addTo(newsListView).setOnItemClickListener((recyclerView, position, v) -> {
            String newsId = adapter.getNewsByPosition(position).getId();
            View imageView = v.findViewById(R.id.image_view);
            if (imageView != null) {
                imageView.setTransitionName("image");
                ActivityOptionsCompat options =  ActivityOptionsCompat
                        .makeSceneTransitionAnimation(getBaseActivity(),
                                v.findViewById(R.id.image_view),
                                "image");
                startActivity(ReaderActivity.newIntent(getContext(),
                        newsId,
                        adapter.getNewsByPosition(position).getImage(),
                        topic),options.toBundle());

            } else {
                startActivity(ReaderActivity.newIntent(getContext(),
                        newsId,
                        adapter.getNewsByPosition(position).getImage(),
                        topic));
            }
        });
        swipeRefreshLayout.setOnRefreshListener(() -> {
            getNews(true);
            changeState(FragmentState.LOAD);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
        getNews(false);
    }

    @Override
    public void onDestroyView() {
        subscription.unsubscribe();
        if (snackbar != null)
            snackbar.dismiss();
        super.onDestroyView();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_news;
    }

    @Override
    protected void onHandleArguments(@NonNull final Bundle arguments) {
        topic = Topic.values()[arguments.getInt(ARGUMENT_KEY)];
    }

    private void getNews(boolean refresh) {
        subscription = newsController.getNews(topic, refresh)
                .subscribe(newsList -> {
                            bindNewsList(newsList);
                            changeState(FragmentState.READ);
                        },
                        error -> {
                            changeState(FragmentState.ERROR);
                            showSnack(error);
                        });
    }

    private void bindNewsList(List<News> newsList) {
        adapter = new NewsAdapter(getContext(), newsList, favorite);
        if (newsListView.getAdapter() != null) {
            newsListView.swapAdapter(adapter, true);
        } else {
            newsListView.setAdapter(adapter);
        }
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void showSnack(Throwable error) {
        snackbar = showSnack(Api.getErrorText(error),
                R.string.repeat,
                v -> {
                    changeState(FragmentState.LOAD);
                    getNews(true);
                });
    }

    private void changeState(FragmentState newState) {
        if (newState != state) {
            switch (newState) {
                case LOAD:
                    lockLayout.setVisibility(View.VISIBLE);
                    errorText.setVisibility(View.INVISIBLE);
                    break;
                case READ:
                     lockLayout.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                    if (swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    if (adapter.getItemCount() == 0) {
                        errorText.setVisibility(View.VISIBLE);
                    }else{
                        errorText.setVisibility(View.INVISIBLE);
                    }
                    break;
                case ERROR:
                    lockLayout.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                    if (swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    errorText.setVisibility(View.VISIBLE);
                    break;
            }
        }
        state = newState;
    }

}
