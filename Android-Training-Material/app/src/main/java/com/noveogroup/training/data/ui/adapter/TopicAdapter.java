package com.noveogroup.training.data.ui.adapter;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.noveogroup.trainig.fragments.R;
import com.noveogroup.training.data.data.model.Topic;
import com.noveogroup.training.data.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * Created by alobynya on 07-Apr-17.
 */
public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.ViewHolder> {

    static Logger logger = LoggerFactory.getLogger("COLOR");
    private int selectedItemPosition = 0;
    private Checkable selectedItem;
    private Context themedContext;

    public TopicAdapter(final Context context) {
        Configuration config = context.getResources().getConfiguration();
        themedContext = new ContextWrapper(context).createConfigurationContext(config);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        themedContext.setTheme(Topic.values()[viewType].style);
        View view = LayoutInflater.from(themedContext).inflate(R.layout.tags_list_element,
                viewGroup,
                false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Topic topic = Topic.values()[position];
        viewHolder.topic.setText(topic.title);
        if (topic == Topic.FAVORITE) {
            viewHolder.topic.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_star, 0, 0, 0);
        } else {
            viewHolder.topic.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_circle, 0, 0, 0);
        }
        int color = ContextCompat.getColor(viewHolder.topic.getContext(),
                Utils.getColor(viewHolder.topic.getContext(), R.attr.colorPrimary));
        viewHolder.topic.getCompoundDrawables()[0].setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        if (selectedItemPosition == position) {
            viewHolder.topic.setChecked(true);
            selectedItem = viewHolder.topic;
        } else {
            viewHolder.topic.setChecked(false);
        }
        StateListDrawable res = new StateListDrawable();
        ColorDrawable drawable = new ColorDrawable(color);
        drawable.setAlpha(themedContext.getResources().getInteger(R.integer.topic_alpha));
        res.addState(new int[]{android.R.attr.state_checked}, drawable);
        res.addState(new int[]{}, new ColorDrawable(Color.WHITE));
        ((RippleDrawable) viewHolder.topic.getBackground()).setDrawableByLayerId(R.id.selector, res);
    }

    @Override
    public int getItemCount() {
        return Topic.values().length;
    }

    public void setItemSelected(int position, View view) {
        selectedItem.setChecked(false);
        selectedItemPosition = position;
        selectedItem = (Checkable) view;
        selectedItem.setChecked(true);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tag_value)
        CheckedTextView topic;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}