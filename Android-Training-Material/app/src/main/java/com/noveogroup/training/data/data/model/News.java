package com.noveogroup.training.data.data.model;

import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class News {
    @SerializedName("id")
    private String id;
    @SerializedName("image")
    private String image;
    @SerializedName("title")
    private String title;
    @SerializedName("pubDate")
    private DateTime date;
    @SerializedName("topics")
    private List<Topic> topics;
}
