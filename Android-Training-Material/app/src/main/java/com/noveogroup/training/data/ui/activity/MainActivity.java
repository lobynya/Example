package com.noveogroup.training.data.ui.activity;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import com.noveogroup.trainig.fragments.R;
import com.noveogroup.training.data.ui.fragment.BaseFragment;
import com.noveogroup.training.data.ui.fragment.LeftMenuFragment;
import com.noveogroup.training.data.ui.fragment.NewsFragment;
import com.noveogroup.training.data.util.FragmentUtils;

public class MainActivity extends BaseActivity {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((LeftMenuFragment) getSupportFragmentManager().findFragmentById(R.id.left_menu)).init(toolbar, drawerLayout);
        if (savedInstanceState == null) {
            replaceFragment(NewsFragment.newInstance(0),
                    true,
                    FragmentUtils.AnimationType.FROM_RIGHT);
        }
    }

    @Override
    public void replaceFragment(final BaseFragment nextFragment, final boolean replace,
                                final FragmentUtils.AnimationType animation) {
        FragmentUtils.runTransaction(
                getSupportFragmentManager(),
                R.id.fragment_container_news,
                nextFragment, replace, true,
                animation,
                null);
    }

    @Override
    public void themeInit() {
        setTheme(R.style.mainStyle);
    }
}
