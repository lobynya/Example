package com.noveogroup.training.data.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.TextView;
import butterknife.BindView;
import com.noveogroup.trainig.fragments.R;
import com.noveogroup.training.data.ui.adapter.GalleryAdapter;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by alobynya on 24-Apr-17.
 */
public class GalleryActivity extends BaseActivity {
    private static final String TITLE_KEY = "com.noveogroup.training.activity.GalleryFragment.title_key";
    private static final String IMAGE_KEY = "com.noveogroup.training.activity.GalleryFragment.image_key";
    @BindView(R.id.gallery_list)
    ViewPager viewPager;
    @BindView(R.id.news_title)
    TextView titleView;
    @BindView(R.id.counter)
    TextView counter;
    private List<String> images;
    private String title;

    static public Intent newInstance(Context context, ArrayList<String> images, String title) {
        Intent intent = new Intent(context, GalleryActivity.class);
        intent.putExtra(TITLE_KEY, title);
        intent.putStringArrayListExtra(IMAGE_KEY, images);
        return intent;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_gallery;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        titleView.setText(title);
        counter.setText(getString(R.string.from, 1, images.size()));
        PagerAdapter pagerAdapter = new GalleryAdapter(getSupportFragmentManager(), images);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(final int position) {
                counter.setText(getString(R.string.from, position + 1, images.size()));
            }

            @Override
            public void onPageScrollStateChanged(final int state) {
            }
        });
    }

    @Override
    protected void onHandleExtras(@NonNull final Bundle extras) {
        images = extras.getStringArrayList(IMAGE_KEY);
        title = extras.getString(TITLE_KEY);
    }

    @Override
    public void themeInit() {
        setTheme(R.style.galleryStyle);
    }
}
