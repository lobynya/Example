package com.noveogroup.training.data.data.controller;

import android.transition.Transition;
import android.transition.TransitionManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.noveogroup.training.data.data.model.NewsDetails;
import rx.Observable;

import java.util.Queue;

/**
 * Created by alobynya on 19-May-17.
 */
public class ReaderFragmentController {
    private volatile Queue<Observable<NewsDetails>> queue;
    private volatile boolean ready = false;
    private ImageView imageView;
    private TextView date;
    private TextView details;

    ReaderFragmentController(Transition enterTransition) {
        enterTransition.addListener(new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(final Transition transition) {

            }

            @Override
            public void onTransitionEnd(final Transition transition) {
                ready = true;
            }

            @Override
            public void onTransitionCancel(final Transition transition) {

            }

            @Override
            public void onTransitionPause(final Transition transition) {

            }

            @Override
            public void onTransitionResume(final Transition transition) {

            }
        });
    }

    public void push(NewsDetails newsDetails) {
        queue.add(Observable.just(newsDetails));
        if (ready) {
            ready = false;
            next();
        }
    }

    private void next() {
        queue.remove().doOnNext(newsDetails -> {
            //bindView
        });
        if (queue.isEmpty()) {
            ready = true;
        } else {
            next();
        }
    }
}
