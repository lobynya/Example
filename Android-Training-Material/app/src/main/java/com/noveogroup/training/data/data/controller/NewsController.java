package com.noveogroup.training.data.data.controller;

import android.content.Context;
import com.noveogroup.training.data.data.model.News;
import com.noveogroup.training.data.data.model.Topic;
import com.noveogroup.training.data.data.network.Api;
import com.noveogroup.training.data.data.preference.Favorite;
import com.noveogroup.training.data.di.Injector;
import rx.Observable;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by alobynya on 28-Apr-17.
 */
@Singleton
public class NewsController {
    private Api api;
    private Favorite favorite;
    private Observable<List<News>> newsList;

    @Inject
    public NewsController(Api api, Favorite favorite) {
        this.api = api;
        this.favorite = favorite;
    }

    public Observable<List<News>> getNews(Topic topic, boolean refresh) {
        return Observable.defer(() -> {
            if (refresh || newsList == null) {
                return doRequest()
                        .flatMap(Observable::from)
                        .filter(news -> filterFunc(news, topic))
                        .toList();
            } else {
                return newsList
                        .flatMap(Observable::from)
                        .filter(news -> filterFunc(news, topic))
                        .toList();
            }
        });
    }

    private boolean filterFunc(News news, Topic topic) {
        if (topic != Topic.FAVORITE) {
            return news.getTopics().contains(topic);
        } else {
            return favorite.contains(news.getId());
        }
    }

    private Observable<List<News>> doRequest() {
        newsList = api.getAll()
                .map(newsList -> {
                    Collections.reverse(newsList);
                    return newsList;
                })
                .cache();
        return newsList;
    }
}




