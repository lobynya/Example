package com.noveogroup.training.data.data.model;

import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NewsDetails{
    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("images")
    private ArrayList<String> image;
    @SerializedName("pubDate")
    private DateTime date;
    @SerializedName("html")
    private String description;
    @SerializedName("topics")
    private List<Topic> topics;
}
