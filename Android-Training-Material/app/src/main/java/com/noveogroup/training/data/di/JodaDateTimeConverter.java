package com.noveogroup.training.data.di;

import com.google.gson.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Type;
import java.util.Locale;

/**
 * Created by alobynya on 03-May-17.
 */
class JodaDateTimeConverter implements JsonDeserializer<DateTime>, JsonSerializer<DateTime> {
    //server date format "dd MMM yyyy HH:mm:ss Z"

    @Override
    public DateTime deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd MMM yyyy HH:mm:ss Z");
        try {
            return formatter.withLocale(Locale.ENGLISH).parseDateTime(json.getAsString());
        } catch (Exception exception) {
            return DateTime.now();
        }
    }

    @Override
    public JsonElement serialize(final DateTime src, final Type typeOfSrc, final JsonSerializationContext context) {
        return null;
    }
}