package com.noveogroup.training.data.data.preference;

import android.content.Context;
import android.content.SharedPreferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.functions.Action1;

import javax.inject.Inject;
import java.util.Map;

/**
 * Created by alobynya on 25-Apr-17.
 */
public class Favorite {
    private Context context;

    @Inject
    public Favorite(Context context) {
        this.context = context;
    }

    public void addNews(String newsId) {
        editPreferences(context, editor -> editor.putString(newsId, newsId));
    }

    public void deleteNews(String newsId) {
        editPreferences(context, editor -> editor.remove(newsId));
    }

    public boolean contains(String newsId) {
        return getPreferences(context).contains(newsId);
    }

    private SharedPreferences getPreferences(final Context context) {
        return context.getSharedPreferences(context.getApplicationContext().getPackageName(), Context.MODE_PRIVATE);
    }

    private void editPreferences(final Context context, final Action1<SharedPreferences.Editor> action) {
        final SharedPreferences.Editor editor = getPreferences(context).edit();
        action.call(editor);
        editor.apply();
    }
}
