package com.noveogroup.training.data.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionManager;
import android.view.Menu;
import android.view.MenuItem;
import butterknife.BindView;
import com.noveogroup.trainig.fragments.R;
import com.noveogroup.training.data.data.model.Topic;
import com.noveogroup.training.data.data.preference.Favorite;
import com.noveogroup.training.data.ui.fragment.BaseFragment;
import com.noveogroup.training.data.ui.fragment.ReaderFragment;
import com.noveogroup.training.data.util.FragmentUtils;
import com.noveogroup.training.data.di.Injector;
import com.noveogroup.training.data.util.Utils;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class ReaderActivity extends BaseActivity {
    private static final String INTENT_NEWS_ID = "com.noveogroup.training.fragments.news_Id";
    private static final String INTENT_TOPIC_ID = "com.noveogroup.training.fragments.topic_Id";
    private static final String INTENT_IMAGE = "com.noveogroup.training.fragments.image";

    @Inject
    Favorite favorite;
    private String newsId;
    private String image;
    private Topic topic;

    static public Intent newIntent(Context context, String newsId, String image, Topic topic) {
        Intent intent = new Intent(context, ReaderActivity.class);
        intent.putExtra(INTENT_NEWS_ID, newsId);
        intent.putExtra(INTENT_IMAGE, image);
        intent.putExtra(INTENT_TOPIC_ID, topic.ordinal());
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Injector.createReaderComponent();
        Injector.getReaderComponent().inject(this);
        if(savedInstanceState == null) {
            replaceFragment(ReaderFragment.newInstance(newsId, image),
                    true,
                    FragmentUtils.AnimationType.FROM_BOTTOM);
        }
    }

    @Override
    public void finish() {
        Injector.deleteReaderComponent();
        super.finish();
    }

    @Override
    public void themeInit() {
        setTheme(topic.style);
    }


    @Override
    public void replaceFragment(final BaseFragment nextFragment, final boolean replace,
                                final FragmentUtils.AnimationType animation) {
        FragmentUtils.runTransaction(
                getSupportFragmentManager(),
                R.id.fragment_container,
                nextFragment, replace, true,
                animation,
                null);
    }

    protected int getLayoutId() {
        return R.layout.activity_reader;
    }

    protected void onHandleExtras(@NonNull final Bundle extras) {
        newsId = extras.getString(INTENT_NEWS_ID);
        topic = Topic.values()[extras.getInt(INTENT_TOPIC_ID)];
        image = extras.getString(INTENT_IMAGE);
    }

}

