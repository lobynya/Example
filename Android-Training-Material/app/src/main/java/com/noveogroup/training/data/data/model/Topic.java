package com.noveogroup.training.data.data.model;

import android.support.annotation.StringRes;
import android.support.annotation.StyleRes;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.noveogroup.trainig.fragments.R;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Topic {
    @SerializedName("main")
    MAIN(R.style.mainStyle,
            R.string.title_main),
    @SerializedName("tech")
    TECH(R.style.techStyle,
            R.string.title_tech),
    @SerializedName("policy")
    POLITICS(R.style.policyStyle,
            R.string.title_politics),
    @SerializedName("culture")
    CULTURE(R.style.cultureStyle,
            R.string.title_culture),
    @SerializedName("sport")
    SPORT(R.style.sportStyle,
            R.string.title_sport),
    FAVORITE(R.style.favoriteStyle,
            R.string.favorite);

    @StyleRes
    public int style;
    @StringRes
    public int title;
}

