package com.noveogroup.training.data.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.*;
import android.view.View;
import butterknife.BindView;
import com.noveogroup.trainig.fragments.R;
import com.noveogroup.training.data.data.model.Topic;
import com.noveogroup.training.data.ui.adapter.ItemClickSupport;
import com.noveogroup.training.data.ui.adapter.TopicAdapter;
import com.noveogroup.training.data.util.FragmentUtils;
import com.noveogroup.training.data.util.Utils;

/*
 * Created by alobynya on 18-Apr-17.
 */
public class LeftMenuFragment extends BaseFragment {
    @BindView(R.id.topicList)
    RecyclerView topicList;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private TopicAdapter adapter;

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        topicList.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new TopicAdapter(getContext());
        topicList.setAdapter(adapter);
        ItemClickSupport.addTo(topicList).setOnItemClickListener((recyclerView, position, v) -> {
            adapter.setItemSelected(position, v);
            closeDrawer();
            Utils.changeTheme(getContext(), Topic.values()[position], getBaseActivity());
            getBaseActivity().replaceFragment(NewsFragment.newInstance(position),
                    true,
                    FragmentUtils.AnimationType.FROM_RIGHT);
        });
        topicList.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onResume() {
        super.onResume();
        drawerToggle.syncState();
    }

    public void init(final Toolbar toolbar, final DrawerLayout drawerLayout) {
        this.drawerLayout = drawerLayout;
        drawerToggle = new ActionBarDrawerToggle(getBaseActivity(),
                drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        this.drawerLayout.addDrawerListener(drawerToggle);
    }

    private void closeDrawer() {
        new Handler().postDelayed(() -> drawerLayout.closeDrawer(GravityCompat.START, true), getContext().getResources().getInteger(R.integer.animation_time) / 2);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_left_menu;
    }
}
