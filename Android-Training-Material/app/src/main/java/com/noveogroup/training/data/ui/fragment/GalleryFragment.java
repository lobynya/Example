package com.noveogroup.training.data.ui.fragment;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import butterknife.BindView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.noveogroup.trainig.fragments.R;
import com.noveogroup.training.data.util.Utils;

/**
 * Created by alobynya on 24-Apr-17.
 */
public class GalleryFragment extends BaseFragment {
    private static final String PAGE_KEY = "com.noveogroup.training.fragments.GalleryFragment.key";
    private static final String IMG_KEY = "com.noveogroup.training.fragments.GalleryFragment.key";
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.image_view)
    ImageView imageView;
    private String image;

    public static GalleryFragment newInstance(int pageNumber, String image) {
        GalleryFragment galleryFragment = new GalleryFragment();
        Bundle args = new Bundle();
        args.putInt(PAGE_KEY, pageNumber);
        args.putString(IMG_KEY, image);
        galleryFragment.setArguments(args);
        return galleryFragment;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        Context context = this.getContext();
        Glide.with(context)
                .load(image)
                .error(R.drawable.ic_stub_error)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(final Exception e, final String model, final Target<GlideDrawable> target, final boolean isFirstResource) {
                        int color = ContextCompat.getColor(context,
                                Utils.getColor(context, R.attr.titleTextColor));
                        imageView.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
                        progressBar.setVisibility(View.INVISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(final GlideDrawable resource, final String model, final Target<GlideDrawable> target, final boolean isFromMemoryCache, final boolean isFirstResource) {
                        progressBar.setVisibility(View.INVISIBLE);
                        return false;
                    }
                })
                .into(imageView);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_gallery;
    }

    @Override
    protected void onHandleArguments(@NonNull final Bundle arguments) {
        image = getArguments().getString(IMG_KEY);
    }
}
