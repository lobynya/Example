package com.noveogroup.training.data.data.controller;

import com.noveogroup.training.data.data.model.NewsDetails;
import com.noveogroup.training.data.data.network.Api;
import com.noveogroup.training.data.di.Injector;
import rx.Observable;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by alobynya on 28-Apr-17.
 */

@Injector.Reader
public class DetailNewsController {
    private Api api;
    private Observable<NewsDetails> newsDetails;

    @Inject
    public DetailNewsController(Api api) {
        this.api = api;
    }

    public Observable<NewsDetails> getNewsDetails(String newsId, boolean refresh) {
        return Observable.defer(() -> {
            if (refresh || newsDetails == null) {
                return doRequest(newsId);
            } else {
                return newsDetails;
            }
        });
    }

    private Observable<NewsDetails> doRequest(String newsId) {
        newsDetails = api.getNewsById(newsId).cache();
        return newsDetails;
    }
}
