package com.noveogroup.training.data.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.SwipeDismissBehavior;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.*;
import android.widget.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.noveogroup.trainig.fragments.R;
import com.noveogroup.training.data.data.model.News;
import com.noveogroup.training.data.data.network.Api;
import com.noveogroup.training.data.data.preference.Favorite;
import com.noveogroup.training.data.util.Utils;
import lombok.NonNull;
import org.joda.time.DateTime;
import org.joda.time.Period;

import java.util.List;

/*
 * Created by alobynya on 07-Apr-17.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {
    private static final int NO_IMAGE = 0;
    private static final int HAVE_IMAGE = 1;
    private List<News> records;
    private Context context;
    private Favorite favorite;

    public NewsAdapter(Context context, @NonNull List<News> records, Favorite favorite) {
        this.context = context;
        this.records = records;
        this.favorite = favorite;
    }

    public News getNewsByPosition(int position) {
        return records.get(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;
        switch (viewType) {
            case NO_IMAGE:
                view = LayoutInflater.from(context)
                        .inflate(R.layout.news_item_list_no_image, viewGroup, false);
                return new ViewHolder(view);
            case HAVE_IMAGE:
                view = LayoutInflater.from(context)
                        .inflate(R.layout.news_list_item, viewGroup, false);
                return new ViewHolderImage(view);

            default:
                view = LayoutInflater.from(context)
                        .inflate(R.layout.news_list_item, viewGroup, false);
                return new ViewHolderImage(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (records.get(position).getImage() == null) ? NO_IMAGE : HAVE_IMAGE;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.bind(records.get(position));
    }

    @Override
    public int getItemCount() {
        return records.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.news_title)
        TextView title;
        @BindView(R.id.news_date)
        TextView date;
        @BindView(R.id.favorite_button)
        ImageButton favoriteButton;
        @BindView(R.id.share_button)
        ImageButton shareButton;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(News record) {
            title.setText(record.getTitle());
            DateTime presentTime = DateTime.now();
            date.setText(Utils.howLongAgo(context, new Period(record.getDate(), presentTime)));
            if (favorite.contains(record.getId())) {
                favoriteButton.setImageDrawable(context.getDrawable(R.drawable.ic_like_solid));
            } else {
                favoriteButton.setImageDrawable(context.getDrawable(R.drawable.ic_like_border));
            }
            int color = ContextCompat.getColor(context,
                    Utils.getColor(context, R.attr.colorPrimary));
            favoriteButton.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            shareButton.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            favoriteButton.setOnClickListener(view -> {
                if (favorite.contains(record.getId())) {
                    favorite.deleteNews(record.getId());
                    favoriteButton.setImageDrawable(context.getDrawable(R.drawable.ic_like_border));
                    showSnack(R.string.favorite_del,favoriteButton);
                } else {
                    favorite.addNews(record.getId());
                    favoriteButton.setImageDrawable(context.getDrawable(R.drawable.ic_like_solid));
                    showSnack(R.string.favorite_add,favoriteButton);
                }
            });
            shareButton.setOnClickListener(view -> {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, record.getTitle());
                context.startActivity(Intent.createChooser(sharingIntent, context.getString(R.string.shared_via)));
            });
        }
    }

    class ViewHolderImage extends ViewHolder {
        @BindView(R.id.image_view)
        ImageView image;
        @BindView(R.id.card_view)
        CardView cardView;
        ViewHolderImage(final View itemView) {
            super(itemView);
        }

        @Override
        public void bind(News record) {
            super.bind(record);
            Glide.with(image.getContext())
                    .load(record.getImage())
                    .error(R.drawable.ic_stub_error)
                    .into(image);
            /*final SwipeDismissBehavior<CardView> swipe=
                    new SwipeDismissBehavior<>();
            swipe.setListener(new SwipeDismissBehavior.OnDismissListener() {
                @Override
                public void onDismiss(final View view) {

                }

                @Override
                public void onDragStateChanged(final int state) {

                }
            });
            ((CoordinatorLayout.LayoutParams) cardView.getLayoutParams()).setBehavior(swipe);*/
        }
    }

    private void showSnack(@StringRes int text, View view) {
        Snackbar snackbar = Snackbar.make(view, text, Snackbar.LENGTH_LONG);
        ((TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text))
                .setTextColor(Color.WHITE);
        snackbar.show();
    }
}