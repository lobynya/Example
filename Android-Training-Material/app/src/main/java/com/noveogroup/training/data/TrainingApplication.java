package com.noveogroup.training.data;

import android.app.Application;

import com.noveogroup.training.data.di.Injector;
import com.noveogroup.training.data.util.Utils;
import net.danlew.android.joda.JodaTimeAndroid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrainingApplication extends Application {
    private static final Logger LOGGER = LoggerFactory.getLogger(TrainingApplication.class.getSimpleName());

    @Override
    public void onCreate() {
        super.onCreate();
        Injector.createAppComponent(this);
        Utils.setLocale("ru", "Cyrl");
        JodaTimeAndroid.init(this);
    }
}
