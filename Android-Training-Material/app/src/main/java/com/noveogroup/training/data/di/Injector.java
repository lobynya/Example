package com.noveogroup.training.data.di;

import android.content.Context;
import com.noveogroup.training.data.data.network.Api;
import com.noveogroup.training.data.ui.activity.ReaderActivity;
import com.noveogroup.training.data.ui.fragment.NewsFragment;
import com.noveogroup.training.data.ui.fragment.ReaderFragment;
import dagger.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Scope;
import javax.inject.Singleton;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by alobynya on 28-Apr-17.
 */
public class Injector {
    private static Logger logger = LoggerFactory.getLogger(Injector.class.getName());
    private static AppComponent appComponent;
    private static ReaderComponent readerComponent;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public static void createAppComponent(Context context) {
        appComponent = DaggerInjector_AppComponent.builder()
                .appModule(new AppModule(context))
                .build();
    }

    public static ReaderComponent getReaderComponent() {
        return readerComponent;
    }

    public static void createReaderComponent() {
        logger.debug("createReaderComponent");
        if (readerComponent == null) {
            readerComponent = DaggerInjector_ReaderComponent.builder()
                    .appComponent(appComponent)
                    .build();
        } else {
            logger.debug("don't create second ReaderComponent!");
        }
        logger.debug("good");
    }

    public static void deleteReaderComponent() {
        logger.debug("deleteReaderComponent");
        readerComponent = null;
    }

    @Singleton
    @Component(modules = AppModule.class)
    public interface AppComponent {
        Context context();

        Api api();

        void inject(NewsFragment newsFragment);
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Reader {
    }

    @Reader
    @Component(dependencies = AppComponent.class)
    public interface ReaderComponent {
        void inject(ReaderFragment readerFragment);

        void inject(ReaderActivity readerActivity);
    }
}
