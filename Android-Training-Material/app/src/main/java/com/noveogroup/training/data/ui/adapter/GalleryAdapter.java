package com.noveogroup.training.data.ui.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.noveogroup.training.data.ui.fragment.GalleryFragment;

import java.util.List;

/**
 * Created by alobynya on 24-Apr-17.
 */
public class GalleryAdapter extends FragmentPagerAdapter {
    private List<String> images;

    public GalleryAdapter(final FragmentManager fragmentManager, List<String> images) {
        super(fragmentManager);
        this.images = images;
    }

    @Override
    public Fragment getItem(final int position) {
        return GalleryFragment.newInstance(position, images.get(position));
    }

    @Override
    public int getCount() {
        return images.size();
    }
}
