package com.noveogroup.training.data.util;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;

import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.TypedValue;
import android.view.MenuItem;
import com.noveogroup.trainig.fragments.R;
import com.noveogroup.training.data.data.model.Topic;
import com.noveogroup.training.data.ui.activity.BaseActivity;
import org.joda.time.DateTime;
import org.joda.time.Period;

import java.text.SimpleDateFormat;
import java.util.Locale;

/*
 * Created by alobynya on 11-Apr-17.
 */
public class Utils {
    private static SimpleDateFormat myFormat;

    {
        Locale locale = new Locale.Builder().setLanguage("ru").setScript("Cyrl").build();
        myFormat = new SimpleDateFormat("E, MMM d, H:mm", locale);
    }

    public static int getColor(Context context, int idAttr) {
        final TypedValue value = new TypedValue();
        return context.getTheme().resolveAttribute(
                idAttr, value, true) ? value.resourceId : R.color.base;
    }

    public static void setLocale(String language, String Script) {
        Locale locale = new Locale.Builder().setLanguage(language).setScript(Script).build();
        myFormat = new SimpleDateFormat("E, MMM d, H:mm", locale);
    }

    public static String dateToString(DateTime date) {
        return myFormat.format(date.toDate());
    }

    public static String howLongAgo(Context context, Period time) {
        Resources res = context.getResources();
        if (time.getYears() > 0) {
            return res.getQuantityString(R.plurals.year, time.getYears(), time.getYears());
        }
        if (time.getMonths() > 0) {
            return res.getQuantityString(R.plurals.month, time.getMonths(), time.getMonths());
        }
        if (time.getDays() > 0) {
            return res.getQuantityString(R.plurals.day, time.getDays(), time.getDays());
        }
        if (time.getHours() > 0) {
            return res.getQuantityString(R.plurals.hour, time.getHours(), time.getHours());
        }
        if (time.getMinutes() > 0) {
            return res.getQuantityString(R.plurals.minutes, time.getMinutes(), time.getMinutes());
        }
        if (time.getSeconds() > 0) {
            return res.getQuantityString(R.plurals.second, time.getSeconds(), time.getSeconds());
        }
        return res.getString(R.string.strange_time);
    }

    public static String fromHtml(String text) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(text).toString();
        }
    }

    public static void changeTheme(Context context, Topic topic, BaseActivity activity) {
        activity.getSupportActionBar().setTitle(topic.title);

        int colorToolBarFrom = ContextCompat.getColor(context, getColor(context, R.attr.colorPrimary));
        int colorStatusBarFrom = ContextCompat.getColor(context, getColor(context, R.attr.colorPrimaryDark));
        context.setTheme(topic.style);
        int colorToolBarTo = ContextCompat.getColor(context, getColor(context, R.attr.colorPrimary));
        int colorStatusBarTo = ContextCompat.getColor(context, getColor(context, R.attr.colorPrimaryDark));
        ValueAnimator toolBarAnimator =
                ValueAnimator.ofObject(new ArgbEvaluator(), colorToolBarFrom, colorToolBarTo);
        ValueAnimator statusBarAnimator =
                ValueAnimator.ofObject(new ArgbEvaluator(), colorStatusBarFrom, colorStatusBarTo);
        toolBarAnimator.addUpdateListener((animator) -> {
            activity.getSupportActionBar().setBackgroundDrawable(
                    new ColorDrawable((Integer) animator.getAnimatedValue()));
        });
        statusBarAnimator.addUpdateListener((animator) -> {
            activity.getWindow().setStatusBarColor(((Integer) animator.getAnimatedValue()));
        });
        toolBarAnimator.setDuration(context.getResources().getInteger(R.integer.animation_time));
        toolBarAnimator.start();
        statusBarAnimator.setDuration(context.getResources().getInteger(R.integer.animation_time));
        statusBarAnimator.start();
    }

    public static void setWhiteIcon(Context context, MenuItem menuItem, @DrawableRes int drawableResId) {
        menuItem.setIcon(ContextCompat.getDrawable(context, drawableResId));
        menuItem.getIcon()
                .setColorFilter(ContextCompat.getColor(context, R.color.white), PorterDuff.Mode.SRC_ATOP);
    }
}

