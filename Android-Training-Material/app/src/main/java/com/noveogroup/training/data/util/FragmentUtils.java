package com.noveogroup.training.data.util;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.noveogroup.trainig.fragments.R;


public final class FragmentUtils {

    private FragmentUtils() {
    }

    public static <T extends Fragment> T withArgs(final T fragment, final Bundle arguments) {
        if (fragment != null) {
            fragment.setArguments(arguments);
        }
        return fragment;
    }

    public static void runTransaction(final FragmentManager fragmentManager,
                                      @IdRes final int containerId,
                                      final Fragment nextFragment,
                                      final boolean replace, final boolean backStack,
                                      final AnimationType animation,
                                      final Fragment removeFragment) {

        if (fragmentManager == null) {
            return;
        }

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        switch (animation) {
            case FROM_BOTTOM:
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_bottom,
                        R.anim.exit_to_top, R.anim.enter_from_top, R.anim.exit_to_bottom);
                break;
            case FADE:
                fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out,
                        R.anim.fade_in, R.anim.fade_out);
                break;
            case FROM_RIGHT:
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right,
                        R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                break;
            case ROTATE:
                fragmentTransaction.setCustomAnimations(R.anim.rotate_right_in,
                        R.anim.rotate_right_out, R.anim.rotate_left_in, R.anim.rotate_left_out);
                break;
            case NO_ANIMATION:
                break;
            default:
                break;
        }

        if (nextFragment != null) {
            final String tag = nextFragment.getClass().getSimpleName();

            if (replace) {
                fragmentTransaction.replace(containerId, nextFragment);
            } else {
                fragmentTransaction.add(containerId, nextFragment, tag);
            }

            if (backStack) {
                fragmentTransaction.addToBackStack(tag);
            }
        }

        if (removeFragment != null) {
            fragmentTransaction.remove(removeFragment);
        }

        fragmentTransaction.commit();
    }

    public enum AnimationType {
        FROM_BOTTOM, FROM_RIGHT, ROTATE, FADE, NO_ANIMATION
    }

}
