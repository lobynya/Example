package com.noveogroup.training.data.data.network;

import com.noveogroup.training.data.data.model.News;
import com.noveogroup.training.data.data.model.NewsDetails;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

import java.util.List;

public interface NewsService {
    //do your requests, use Observables.

    @GET("news/getAll")
    Observable<List<News>> getAll();

    @GET("news/getById")
    Observable<NewsDetails> getNewsById(@Query("id") String newsId);
}
